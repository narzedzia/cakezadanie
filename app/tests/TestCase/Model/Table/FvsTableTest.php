<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FvsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FvsTable Test Case
 */
class FvsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FvsTable
     */
    public $Fvs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Fvs',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fvs') ? [] : ['className' => FvsTable::class];
        $this->Fvs = TableRegistry::getTableLocator()->get('Fvs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fvs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
