var gulp = require('gulp');
var path = require('path');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var open = require('gulp-open');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var debug = require('gulp-debug');

var Paths = {
  HERE: './',
  DIST: 'dist/',
  CSS: './../webroot/css/',
  SCSS_TOOLKIT_SOURCES: './assets/scss/material-dashboard.scss',
  SCSS: './assets/scss/**/**'
};

var config = {
	    jsSrc: [
	    	 './assets/js/core/jquery.min.js',
	    	 './assets/js/core/popper.min.js',
	    	 './assets/js/core/bootstrap-material-design.min.js',
	    	 './assets/js/plugins/perfect-scrollbar.jquery.min.js',
	    	 './assets/js/plugins/moment.min.js',
	    	 './assets/js/plugins/sweetalert2.js',
	    	 './assets/js/plugins/jquery.validate.min.js',
	    	 './assets/js/plugins/jquery.bootstrap-wizard.js',
	    	 './assets/js/plugins/bootstrap-selectpicker.js',
	    	 './assets/js/plugins/bootstrap-datetimepicker.min.js',
	    	 './assets/js/plugins/jquery.datatables.min.js',
	    	 './assets/js/plugins/bootstrap-tagsinput.js',
	    	 './assets/js/plugins/jasny-bootstrap.min.js',
	    	 './assets/js/plugins/fullcalendar.min.js',
	    	 './assets/js/plugins/jquery-jvectormap.js',
	    	 './assets/js/plugins/nouislider.min.js',
	    	 './assets/js/plugins/arrive.min.js',
	    	 './assets/js/plugins/chartist.min.js',
	    	 './assets/js/plugins/bootstrap-notify.j',
	    	 './assets/js/material-dashboard.min.js',
	        './../webroot/js/my.js',
	    ],
	    jsDst: './../webroot/js/',
	};

gulp.task('compile-scss', function() {
  return gulp.src(Paths.SCSS_TOOLKIT_SOURCES)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write(Paths.HERE))
    .pipe(gulp.dest(Paths.CSS));
});

gulp.task('js', function() {
	return gulp.src(config.jsSrc)
        .pipe(debug())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('script.min.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.jsDst))
    ;
});

gulp.task('watch', function() {
  gulp.watch(Paths.SCSS, ['compile-scss']);
  gulp.watch(config.jsDst+'my.js', ['js']);
});

gulp.task('open', function() {
  gulp.src('examples/dashboard.html')
    .pipe(open());
});

gulp.task('open-app', ['open', 'watch']);