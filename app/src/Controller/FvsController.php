<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Hashids\Hashids;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/** 
 * Fvs Controller
 *
 * @property \App\Model\Table\FvsTable $Fvs
 *
 * @method \App\Model\Entity\Fv[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FvsController extends AppController
{
    
    public $paginateOptions = [
        'limit' => 1,
    ];
    
    public $helpers = [
        'ListFilter.ListFilter'
    ];
    
    public $components = [
        'ListFilter.ListFilter'
    ];
    
    
    
    public function getListFilters($action) {
        $filters = [];
        if ($action == 'index') {
            $filters = [
                'fields' => [
                    'Fv.email' => [
                        'searchType' => 'wildcard',
                        'inputOptions' => [
                            'label' => 'Fv email: '
                        ]
                    ],
                    'Fv.user_id' => [
                        'searchType' => 'select',
                        'options' => $this->Fvs->Users->find('list'),
                        'inputOptions' => [
                            'label' => 'Created By: '
                        ]
                    ],
    
                ]
            ];
        }
    
        return $filters;
    }
    
    
    public function isAuthorized($user = null)
    {
        // not access this route for admin
        if (isset($user['role']) && $user['role'] === 'admin') {
            // if (in_array($this->request->getParam('action'), ['manage','edit','delete'])) {
            //     return false;
            //  }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $fvs = $this->paginate($this->Fvs,$this->paginateOptions);

        $this->set(compact('fvs'));
    }

    /**
     * View method
     *
     * @param string|null $id Fv id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($eid = null)
    {
    
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
        $id = $hashids->decodeHex($eid);

        $fv = $this->Fvs->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('fv', $fv);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fv = $this->Fvs->newEntity();
        if ($this->request->is('post')) {
            $fv = $this->Fv->patchEntity($fv, $this->request->getData(), [
                'validate' => true
            ]);
            $fv->user_id = $this->Auth->user('id');
            if ($this->Fvs->save($fv)) {
                $this->Flash->success(__('The fv has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fv could not be saved. Please, try again.'));
        }
        $users = $this->Fvs->Users->find('list', ['limit' => 200]);
        $this->set(compact('fv', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Fv id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($eid)
    {
    
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
        $id = $hashids->decodeHex($eid);
        
        $fv = $this->Fvs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fv = $this->Fvs->patchEntity($fv, $this->request->getData());
            if ($this->Fvs->save($fv)) {
                $this->Flash->success(__('The fv has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fv could not be saved. Please, try again.'));
        }
        $users = $this->Fvs->Users->find('list', ['limit' => 200]);
        $this->set(compact('fv', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Fv id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($eid)
    {
        $this->request->allowMethod(['post', 'delete']);
    
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
        $id = $hashids->decodeHex($eid);
        
        $this->request->allowMethod(['post', 'delete']);
        $fv = $this->Fvs->get($id);
        if ($this->Fvs->delete($fv)) {
            $this->Flash->success(__('The fv has been deleted.'));
        } else {
            $this->Flash->error(__('The fv could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
