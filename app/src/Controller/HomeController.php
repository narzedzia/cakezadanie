<?php
namespace App\Controller;

use Cake\Event\Event;

class HomeController extends AppController
{
    public function beforeFilter(Event $event)
    {
        $this->set('title', 'Dashboard');
        parent::beforeFilter($event);
        // allow this route/action
       // $this->Auth->allow(['index']);
    }

    public function index()
    {   
        
    }

}