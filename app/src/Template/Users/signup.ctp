<div class="row justify-content-center">
 <div class="col-12 col-sm-6 col-lg-4">
<div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">User Sign Up Here</h4>
                </div>
                <div class="card-body">
    <?php 
        echo $this->Flash->render();
        
        $this->Form->setTemplates([
            'inputContainer' => '<div class="form-group{{required}}"> {{content}} <span class="help">{{help}}</span></div>',
            'input' => '<input type="{{type}}" name="{{name}}" class="form-control form-control-danger" {{attrs}}/>',
            'inputContainerError' => '<div class="form-group has-danger {{type}}{{required}}">{{content}}{{error}}</div>',
            'error' => '<div class="text-danger">{{content}}</div>'
        ]);
        
        echo $this->Form->create($sign_up);
        echo $this->Form->controls(
            [
                'name'      => ['required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'User Full Name']],
                'username'  => ['required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating','text' => 'User Username']],
                'email'     => ['required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating','text' => 'User Email']],
                'password'  => ['required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating']],
                'confirm_password' => ['type' => 'password', 'required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating' ,'text' => 'Enter Confirm Password']],
                'phone'     => ['required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating']],
                'zipcode'   => ['required'  => false, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating']],
            ],
            [ 'legend' => '']
        );
        echo $this->Form->button('<i class="fa fa-user"></i> SignUp',['class' => 'btn btn-success btn-block']);
        echo $this->Form->end(); 
    ?>
    </div>
</div>
</div>
</div>

