<div class="row justify-content-center">
 <div class="col-12 col-sm-6 col-lg-4">


<div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Login</h4>
                </div>
                <div class="card-body">
    <?= $this->Html->css(captcha_layout_stylesheet_url(), ['inline' => false]) ?>
    <?php
        echo $this->Flash->render();

        $this->Form->setTemplates([
            'inputContainer' => '<div class="col-12">  <div class="form-group bmd-form-group"> <div class="form-group{{required}}"> {{content}} <span class="help">{{help}}</span></div></div> </div>',
            'input' => '<input type="{{type}}" name="{{name}}" class="form-control form-control-danger" {{attrs}}/>',
            'inputContainerError' => '<div class="form-group has-danger {{type}}{{required}}">{{content}}{{error}}</div>',
            'error' => '<div class="text-danger">{{content}}</div>'
        ]);
        echo $this->Form->create($login);
        echo $this->Form->controls(
            [
                'email'     => ['required'  => FALSE, 'placeholder' => '', 'label' => ['text' => 'User Email' , 'class'=>'bmd-label-floating']],
                'password'  => ['required'  => FALSE, 'placeholder' => '', 'label' => ['text' => 'Password' , 'class'=>'bmd-label-floating']],
            ],
            [ 'legend' => '']
        );
        // show captcha image html
        echo captcha_image_html();

        // Captcha code user input textbox
        echo $this->Form->input('CaptchaCode', [
            'label' => 'Retype the characters from the picture:',
            'maxlength' => '10',
            'style' => '',
            'id' => 'CaptchaCode',
            'label' => ['text' => 'Enter Image Text', 'class'=>'bmd-label-floating'],
        ]);
        echo $this->Form->button('<i class="fa fa-user"></i> Login',['class' => 'btn btn-success btn-block']);
        echo $this->Form->end(); 
    ?>         
    </div>
</div>


    </div>
</div>