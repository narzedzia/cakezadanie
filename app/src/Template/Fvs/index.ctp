
<div class="row">
    <div class="col-lg-12">
     <?php 
       echo $this->Html->link('Add new',[	'controller' => 'Fvs', 'action' => 'add', 'plugin' => null,  ],[      'class' => 'btn btn-primary']);
    ?>
    </div>
        <div class="col-lg-12">
    
    <?= $this->ListFilter->renderFilterbox() ?>
    </div>
 </div>
    
    
   <div class="row">
    <div class="col-lg-12"> 
    
        <div class="card">
      
            <div class="card-header card-header-primary">
                  <h4 class="card-title "><i class="fa fa-cogs"></i>&nbsp; Manage Fv</h4>
                  <p class="card-category"></p>
                </div>
            
            
            <div class="card-body">
                <?= $this->Flash->render(); ?>
                
          <?php if(!empty(iterator_count($fvs))) { ?>
     <table class="table">
            <thead class="text-primary">
            <tr>
                <th>#</th>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id','Created By:') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fvCreated') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
         <?php
            $i = 1;
            foreach ($fvs as $key => $fv) { 
          ?>
            <tr>
              <td><?= $i++ ?></td>
                <td><?= $this->Number->format($fv->id) ?></td>
                <td><?= h($fv->email) ?></td>
                <td><?= h($fv->user->email) ?></td>
                <td><?= h($fv->name) ?></td>
                <td><?= h($fv->number) ?></td>
                <td><?= h($fv->address) ?></td>
                <td><?= $this->Number->format($fv->price) ?></td>
                <td><?= h($fv->fvCreated) ?></td>
                <td><?= h($fv->created) ?></td>
                <td><?= h($fv->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fv->EId],[      'class' => 'btn btn-info']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fv->EId],[      'class' => 'btn btn-secondary']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fv->EId], ['class' => 'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $fv->id)]) ?>
                </td>
            </tr>
           <?php } ?>
        </tbody>
    </table>

    <nav aria-label="Page navigation example">
  <ul class="pagination">
      <?= $this->Paginator->first('<< ' . __('first')) ?></li>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        </nav>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
 
                <?php }else{ ?>
                    <?= $this->Html->image('no-results.svg', ['alt' => 'No Data Found.','style' => 'height: 250px; width: 100%; display: block;']) ?>
                    <h2 style="text-align:center;"><b style="color:red;">Sorry? </b> No Data Found!</h2>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
