<div class="row">
    <div class="col-lg-12">
     <?php 
       echo $this->Html->link(__('List Fv'),[	'controller' => 'Fvs', 'action' => 'index', 'plugin' => null,  ],[      'class' => 'btn btn-primary']);
    ?>
    </div>  
 </div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <h3 class="card-header"><i class="fa fa-plus-square-o"></i>&nbsp; Add Fv</h3>
             <div class="card-body">
            <?php 
                echo $this->Flash->render();          
                $this->Form->setTemplates([
                    'inputContainer' => '<div class="form-group{{required}}"> {{content}} <span class="help">{{help}}</span></div>',
                    'input' => '<input type="{{type}}" name="{{name}}" class="form-control form-control-danger" {{attrs}}/>',
                    'inputContainerError' => '<div class="form-group has-danger {{type}}{{required}}">{{content}}{{error}}</div>',
                    'error' => '<div class="text-danger">{{content}}</div>',
                    'textarea' => '<textarea  name="{{name}}" class="form-control" {{attrs}}>{{value}}</textarea>',
                ]);
                
                echo $this->Form->create($fv);
                echo $this->Form->controls(
                    [
                        'number' => ['required'  => true, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Number']],
    					'name' => ['required'  => true, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Name']],
    					'email' => ['required'  => true, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Email']],
	                    'price' => ['required'  => true, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Price']],
    					'number' => ['required'  => true, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Number']],
    					'fvCreated' => ['required'  => true, 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Number']],
                        'address'  => ['required'  => false, 'type' => 'textarea', 'placeholder' => '', 'label' => ['class'=> 'bmd-label-floating', 'text' => 'Address'], 'rows' => '10', 'cols' => '15'],
                    ],
                    [ 'legend' => false]
                );
                echo $this->Form->button('<i class="fa fa-plus-circle"></i> Add fv',['class' => 'btn btn-success']);
                echo $this->Form->end(); 
            ?>
            </div>
        </div>
    </div>
</div>
