<div class="row">
    <div class="col-lg-12">
     <?php 
       echo $this->Html->link(__('List Fv'),[	'controller' => 'Fvs', 'action' => 'index', 'plugin' => null,  ],[      'class' => 'btn btn-primary']);
    ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fv->id],
                [ 'class' => 'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $fv->EId)]
            )
        ?>
    </div>  
 </div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <h3 class="card-header">&nbsp; <?= h($fv->name) ?></h3>
             <div class="card-body">
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($fv->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($fv->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number') ?></th>
            <td><?= h($fv->number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($fv->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fv->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Id') ?></th>
            <td><?= h($fv->user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($fv->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FvCreated') ?></th>
            <td><?= h($fv->fvCreated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fv->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fv->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
