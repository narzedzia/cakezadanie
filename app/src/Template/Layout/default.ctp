<?php
    $cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
      <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    
    <?= $this->Html->css(['material-dashboard.css']) ?>
    <?= $this->Html->script(['script.min.js']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <div class="logo">
 
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
          <?=  $this->Html->link('<i class="material-icons">dashboard</i><p>Dashboard</p>', '/', ['class' => 'nav-link', 'escape' => false]); ?>
          </li>
               <?php if ($this->request->session()->read('Auth.User.id')){ ?>
          <li class="nav-item ">
            <a class="nav-link" href="">
              <i class="material-icons">person</i>
              <p>Ustawienia</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="/Fvs">
              <i class="material-icons">content_paste</i>
              <p>Faktury</p>
            </a>
          </li>
             
      <?php }else{ ?>
      
        <li class="nav-item ">
               <?= $this->Html->link('<i class="fa fa-sign-in"></i> Login', '/Login', ['class' => 'nav-link', 'escape' => false]); ?>
          </li>
          
              <li class="nav-item ">
                           <?= $this->Html->link('<i class="fa fa-sign-out"></i> Sign Up', '/Signup', ['class' => 'nav-link', 'escape' => false]); ?>
          </li>
       <?php } ?>
      
        </ul>
      </div>
    </div>
    <div class="main-panel">
    
     <?php if ($this->request->session()->read('Auth.User.id')){ ?>
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
    
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  
                      <?= $this->Html->link('Log out', '/Logout', ['class' => 'dropdown-item', 'escape' => false]); ?>
                  
              
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      
      <?php } ?>
      

     
      <div class="content">
        <div class="container-fluid">

             <?= $this->fetch('content') ?> 
        
          </div>
        </div>
      </div>

 </div>

</body>
</html>
