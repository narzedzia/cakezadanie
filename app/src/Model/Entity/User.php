<?php

namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher; //include this line
use Cake\ORM\Entity;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Hashids\Hashids;

class User extends Entity
{
    protected $_accessible = [
        ' * ' => true,
        'id'    => false,
        'email' => false,
        'password' => false,
        'username' => false,
    ];

    /**
     *  User Password Hash
     */
    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($password);
            // return (new DefaultPasswordHasher)->hash($password);
        }
    }

    protected function _getEId()
    {
        // load hashid config
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
        return $hashids->encodeHex($this->_properties['id']);
    }
}