<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Hashids\Hashids;

/**
 * Fv Entity
 *
 * @property int $id
 * @property string $email
 * @property int $user_id
 * @property string $name
 * @property string $number
 * @property string $address
 * @property float $price
 * @property \Cake\I18n\FrozenTime $fvCreated
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Fv extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'user_id' => true,
        'name' => true,
        'number' => true,
        'address' => true,
        'price' => true,
        'fvCreated' => true,
        'created' => true,
        'modified' => true
    ];
    
    
    protected function _getEId()
    {
        // load hashid config
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
    
        if(!isset($this->_properties['fv']['id']))
            return $hashids->encodeHex($this->_properties['id']);
            else
                return $hashids->encodeHex($this->_properties['fv']['id']);
    }
    
}
